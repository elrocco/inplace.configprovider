﻿using InPlace.ConfigProvider.Model;
using InPlace.ConfigProvider.Queries;
using InPlace.ConfigProvider.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace InPlace.ConfigProvider.Tests.Services
{
    public class FeatureLicencesServiceTests : TestBase
    {
        private readonly Mock<IFeatureLicenceQuery> _featureLicenceQuery;
        private readonly Mock<ITenantQuery> _tenantQuery;
        private readonly Mock<ISignedResponseService> _signedResponseService;
        private readonly FeatureLicencesService _service;
        private readonly List<Tenant> _tenantQueryResult;
        private readonly List<FeatureLicence> _featureLicenceQueryResult;

        public FeatureLicencesServiceTests()
        {
            _tenantQueryResult = new List<Tenant>();
            _featureLicenceQueryResult = new List<FeatureLicence>();
            _featureLicenceQuery = new Mock<IFeatureLicenceQuery>();
            _featureLicenceQuery
                .Setup(x => x.Execute(It.IsAny<FeatureLicenceCriteria>()))
                .Returns(_featureLicenceQueryResult);

            _tenantQuery = new Mock<ITenantQuery>();
            _tenantQuery
                .Setup(x => x.Execute(It.IsAny<TenantCriteria>()))
                .Returns(_tenantQueryResult);

            _signedResponseService = new Mock<ISignedResponseService>();
            _signedResponseService
                .Setup(x => x.GetSignedResponse(It.IsAny<TenantLicenceResponse>()))
                .Returns((TenantLicenceResponse x) => new SignedResponse<TenantLicenceResponse> { Data = x });

            _service = new FeatureLicencesService(_featureLicenceQuery.Object, _tenantQuery.Object, _signedResponseService.Object);
        }

        [Fact]
        public void UniWideLicence()
        {
            _featureLicenceQueryResult.AddRange(new[]
            {
                new FeatureLicence{Region = "AU", Client = "RMIT", FeatureCode = "LogBook"},
                new FeatureLicence{Region = "AU", Client = "MONASH", FeatureCode = "InSight"},
            });

            _tenantQueryResult.AddRange(new[]
            {
                new Tenant{Region = "AU", Client = "RMIT", Environment = "DEV"},
                new Tenant{Region = "AU", Client = "RMIT", Environment = "UAT"},
                new Tenant{Region = "AU", Client = "RMIT", Environment = "PROD"},
                new Tenant{Region = "AU", Client = "MONASH", Environment = "DEV"},
                new Tenant{Region = "AU", Client = "MONASH", Environment = "UAT"},
                new Tenant{Region = "AU", Client = "MONASH", Environment = "PROD"},
            });

            var result = _service.FeatureLicenceResponse("AU", new[] { "InSight", "LogBook" });
            Assert.Equal(6, result.Data.TenantLicences.Count);
            var environments = new[] { "DEV", "UAT", "PROD" };
            foreach (var env in environments)
            {
                AssertArrayEqual(new[] { "LogBook" }, result.Data.TenantLicences[$"AU-RMIT-{env}"]);
            }
            foreach (var env in environments)
            {
                AssertArrayEqual(new[] { "InSight" }, result.Data.TenantLicences[$"AU-MONASH-{env}"]);
            }
        }

        [Fact]
        public void SquashValues()
        {
            _featureLicenceQueryResult.AddRange(new[]
            {
                new FeatureLicence{Region = "AU", Client = "RMIT", FeatureCode = "LogBook"},
                new FeatureLicence{Region = "AU", Client = "RMIT", FeatureCode = "InSight"},
            });

            _tenantQueryResult.AddRange(new[]
            {
                new Tenant{Region = "AU", Client = "RMIT", Environment = "PROD"}
            });

            var result = _service.FeatureLicenceResponse("AU", new[] { "InSight", "LogBook" });
            Assert.Single(result.Data.TenantLicences);
            AssertArrayEqual(new[] { "LogBook", "InSight" }, result.Data.TenantLicences["AU-RMIT-PROD"]);
        }

        private void AssertArrayEqual(string[] expected, string[] actual)
        {
            Assert.Equal(string.Join(",", expected.OrderBy(x => x)), string.Join(",", actual.OrderBy(x => x)));
        }
    }
}
