﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InPlace.ConfigProvider.Commands;
using InPlace.ConfigProvider.Model;
using Xunit;

namespace InPlace.ConfigProvider.Tests.Commands
{
    public class DeleteSettingValuesCommandHandlerTests : DatabaseTestBase<DeleteSettingsValuesCommandHandler>
    {
        [Fact]
        public void DeleteTest()
        {
            Context.Add(new SettingValue { Region = "AU", Client = "DEV", SettingKey = "Testing.Value1", Value = "Testing" });
            Context.Add(new SettingValue { Region = "AU", Client = "PROD", SettingKey = "Testing.Value1", Value = "Testing" });
            Context.Add(new SettingValue { Region = "AU", Client = "DEV", SettingKey = "Testing.Value2", Value = "Testing" });
            Context.SaveChanges();

            var command = new DeleteSettingsValuesCommand
            {
                TenantCode = new TenantCode("AU--DEV"),
                SettingKeys = new List<string> { "Testing.Value1" }
            };

            DbObject.Execute(command);

            Context.RefetchTracked();

            var settingValues = Context.Set<SettingValue>().ToDictionary(x => new Tuple<TenantCode, string>(new TenantCode(x.Region, x.Client, x.Environment), x.SettingKey));
            Assert.False(settingValues.ContainsKey(new Tuple<TenantCode, string>(new TenantCode("AU--DEV"), "Testing.Value1")));
            Assert.True(settingValues.ContainsKey(new Tuple<TenantCode, string>(new TenantCode("AU--PROD"), "Testing.Value1")));
            Assert.True(settingValues.ContainsKey(new Tuple<TenantCode, string>(new TenantCode("AU--DEV"), "Testing.Value2")));

        }
    }
}
