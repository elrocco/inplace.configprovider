﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InPlace.ConfigProvider.Model;
using Xunit;

namespace InPlace.ConfigProvider.Tests.Commands
{
    public class SetSettingsValueCommandHandlerTests : DatabaseTestBase<SetSettingValuesCommandHandler>
    {
        [Fact]
        public void BulkInsertTest()
        {
            var tenantCode = new TenantCode("AU--DEV");
            var command = new SetSettingValuesCommand
            {
                TenantCode = tenantCode,
                Settings = new List<KeyValue>
                {
                    new KeyValue("Setting.Value1", "Value1"),
                    new KeyValue("Setting.Value2", "Value2"),
                    new KeyValue("Setting.Value3", "Value3")
                }
            };

            DbObject.Execute(command);

            var settingValues = Context.Set<SettingValue>().ToDictionary(x => new Tuple<TenantCode, string>(new TenantCode(x.Region, x.Client, x.Environment), x.SettingKey));
            Assert.Equal("Value1", settingValues[new Tuple<TenantCode, string>(tenantCode, "Setting.Value1")].Value);
            Assert.Equal("Value2", settingValues[new Tuple<TenantCode, string>(tenantCode, "Setting.Value2")].Value);
            Assert.Equal("Value3", settingValues[new Tuple<TenantCode, string>(tenantCode, "Setting.Value3")].Value);

        }

        [Fact]
        public void BulkUpdateTest()
        {
            var tenantCode = new TenantCode("AU--PROD");
            var otherTenantCode = new TenantCode("AU--DEV");
            Context.Add(new SettingValue { Region = "AU", Environment = "PROD", SettingKey = "Setting.Key1", Value = "OldVal" });
            Context.Add(new SettingValue { Region = "AU", Environment = "PROD", SettingKey = "Setting.Key2", Value = "OldVal" });
            Context.Add(new SettingValue { Region = "AU", Environment = "PROD", SettingKey = "Setting.Key3", Value = "OldVal" });
            Context.Add(new SettingValue { Region = "AU", Environment = "DEV", SettingKey = "Setting.Key1", Value = "OldVal" });

            Context.SaveChanges();

            var command = new SetSettingValuesCommand
            {
                TenantCode = tenantCode,
                Settings = new List<KeyValue>
                {
                    new KeyValue("Setting.Key1", "NewVal1"),
                    new KeyValue("Setting.Key2", "NewVal2")
                }
            };

            DbObject.Execute(command);

            Context.RefetchTracked();
            var settingValues = Context.Set<SettingValue>().ToDictionary(x => new Tuple<TenantCode, string>(new TenantCode(x.Region, x.Client, x.Environment), x.SettingKey));
            Assert.Equal("NewVal1", settingValues[new Tuple<TenantCode, string>(tenantCode, "Setting.Key1")].Value);
            Assert.Equal("NewVal2", settingValues[new Tuple<TenantCode, string>(tenantCode, "Setting.Key2")].Value);
            Assert.Equal("OldVal", settingValues[new Tuple<TenantCode, string>(tenantCode, "Setting.Key3")].Value);
            Assert.Equal("OldVal", settingValues[new Tuple<TenantCode, string>(otherTenantCode, "Setting.Key1")].Value);


        }
    }
}
