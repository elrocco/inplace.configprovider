﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace InPlace.ConfigProvider.Tests
{
    public class TestDataContext : DbContext
    {
        private readonly DbConnection _connection;

        public TestDataContext(DbContextOptions<TestDataContext> options, DbConnection connection)
            : base(options)
        {
            _connection = connection;
        }


        public void RefetchTracked()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                entry.Reload();
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connection);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var types = GetType().Assembly.GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract && !t.IsGenericType )
                .ToList();

            var baseType = typeof(IEntityTypeConfiguration<>);
            var addConfigurationMethod = this.GetType().GetMethod("AddConfiguration");
            foreach (var type in types)
            {
                var entityConfType = type.GetInterfaces().FirstOrDefault(x => x.IsGenericType && x.GetGenericTypeDefinition() == baseType);
                if (entityConfType == null)
                    continue;

                addConfigurationMethod.MakeGenericMethod(entityConfType.GetGenericArguments()).Invoke(this, new object[]{modelBuilder, Activator.CreateInstance(type)});
            }

        }

        public void AddConfiguration<TEntity>(ModelBuilder modelBuilder, IEntityTypeConfiguration<TEntity> configuration)
             where TEntity : class
        {
            configuration.Configure(modelBuilder.Entity<TEntity>());
        }
    }
}
