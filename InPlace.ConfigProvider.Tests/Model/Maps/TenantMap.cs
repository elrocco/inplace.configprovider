﻿using InPlace.ConfigProvider.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InPlace.ConfigProvider.Tests.Model.Maps
{
    public class TenantMap : IEntityTypeConfiguration<Tenant>
    {
        public void Configure(EntityTypeBuilder<Tenant> builder)
        {
            builder.ToTable("Tenant", "dbo")
                .HasKey(x => x.TenantId);
        }
    }
}
