﻿using InPlace.ConfigProvider.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace InPlace.ConfigProvider.Tests.Model.Maps
{
    public class FeatureLicenceMap : IEntityTypeConfiguration<FeatureLicence>
    {
        public void Configure(EntityTypeBuilder<FeatureLicence> builder)
        {
            builder.ToTable("FeatureLicence", "dbo")
                .HasKey(x => x.FeatureLicenceId);
        }
    }
}
