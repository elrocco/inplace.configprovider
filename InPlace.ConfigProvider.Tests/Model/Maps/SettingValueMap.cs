﻿using InPlace.ConfigProvider.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InPlace.ConfigProvider.Tests.Model.Maps
{
    public class SettingValueMap : IEntityTypeConfiguration<SettingValue>
    {
        public void Configure(EntityTypeBuilder<SettingValue> builder)
        {
            builder.ToTable("SettingValue", "dbo")
                .HasKey(x => x.SettingValueId);
        }
    }
}
