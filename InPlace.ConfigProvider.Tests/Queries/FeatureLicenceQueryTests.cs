﻿using InPlace.ConfigProvider.Model;
using InPlace.ConfigProvider.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace InPlace.ConfigProvider.Tests.Queries
{
    public class FeatureLicenceQueryTests : DatabaseTestBase<FeatureLicenceQuery>
    {
        [Fact]
        public void FilterByRegion()
        {
            var featureLicences = new List<FeatureLicence>
            {
                new FeatureLicence{Region = "AU", Client = "RMIT", FeatureCode = "LogBook"},
                new FeatureLicence{Region = "AU", Client = "MONASH", FeatureCode = "LogBook"},
                new FeatureLicence{Region = "UK", Client = "LEEDS", FeatureCode = "LogBook"},
            };

            featureLicences.ForEach(x => Context.Add(x));
            Context.SaveChanges();

            var result = DbObject.Execute(new FeatureLicenceCriteria { TenantCode = new TenantCode("AU") });
            Assert.Contains(result, x => x.FeatureLicenceId == featureLicences[0].FeatureLicenceId);
            Assert.Contains(result, x => x.FeatureLicenceId == featureLicences[1].FeatureLicenceId);
            Assert.DoesNotContain(result, x => x.FeatureLicenceId == featureLicences[2].FeatureLicenceId);
        }

        [Fact]
        public void FilterByNotExpired()
        {
            var now = DateTimeOffset.Now;
            var licences = new List<FeatureLicence>
            {
                new FeatureLicence{Region = "AU", Client = "RMIT", FeatureCode = "LogBook", ExpiryDate = now.AddDays(-5)},
                new FeatureLicence{Region = "AU", Client = "MONASH", FeatureCode = "LogBook", ExpiryDate = now.AddDays(+5)},
                new FeatureLicence{Region = "AU", Client = "VU", FeatureCode = "LogBook"},
            };

            licences.ForEach(x => Context.Add(x));
            Context.SaveChanges();

            var result = DbObject.Execute(new FeatureLicenceCriteria { TenantCode = new TenantCode("AU"), NotExpired = true });
            Assert.DoesNotContain(result, x => x.FeatureLicenceId == licences[0].FeatureLicenceId);
            Assert.Contains(result, x => x.FeatureLicenceId == licences[1].FeatureLicenceId);
            Assert.Contains(result, x => x.FeatureLicenceId == licences[2].FeatureLicenceId);
        }

        [Fact]
        public void FilterByFeatureCode()
        {
            var licences = new List<FeatureLicence>
            {
                new FeatureLicence{Region = "AU", Client = "RMIT", FeatureCode = "LogBook"},
                new FeatureLicence{Region = "AU", Client = "RMIT", FeatureCode = "InSight"}
            };

            licences.ForEach(x => Context.Add(x));
            Context.SaveChanges();

            var result = DbObject.Execute(new FeatureLicenceCriteria { TenantCode = new TenantCode("AU"), FeatureCodes =new List<string> { "InSight" } });
            Assert.DoesNotContain(result, x => x.FeatureLicenceId == licences[0].FeatureLicenceId);
            Assert.Contains(result, x => x.FeatureLicenceId == licences[1].FeatureLicenceId);
        }
    }
}
