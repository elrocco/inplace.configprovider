﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Extensions.DependencyInjection;
using Dapper;
using InPlace.ConfigProvider.Startup;
using InPlace.Packages.Database;
using InPlace.Packages.Database.Dapper;
using Microsoft.Azure.WebJobs.Description;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace InPlace.ConfigProvider.Tests
{
    public abstract class TestBase : IDisposable
    {
        private SqlConnection _connection;
        private SqlTransaction _transaction;
        public ServiceCollection Services { get; set; }
        public IServiceProvider ServiceProvider { get; set; }
        public TestDataAppSettings AppSettings { get; set; }
        public TestDataContext Context { get; set; }
        public IDbProvider DbProvider{get;set;}

        protected TestBase()
        {
            var appSettingsDirectory = Regex.Replace(Directory.GetCurrentDirectory(), @"(?i)\\bin\\(debug|release)\\netcoreapp\d+.\d+(?-i)", string.Empty);
            var config = new ConfigurationBuilder()
                .SetBasePath(appSettingsDirectory)
                .AddJsonFile("appsettings.json")
                .Build() ;

            Services = new ServiceCollection();
            Services.AddOptions()
                .Configure<TestDataAppSettings>(config)
                ;

            RebuildServiceProvider();

            AppSettings = ServiceProvider.GetService<IOptions<TestDataAppSettings>>().Value;

            _connection = new SqlConnection(AppSettings.ConnectionString);
            _connection.Open();
            _transaction = _connection.BeginTransaction();

            DbProvider = new DbProvider { DbConnection = _connection, DbTransaction = _transaction };
            Context = new TestDataContext(new DbContextOptions<TestDataContext>(), _connection);
            Context.Database.UseTransaction(_transaction);

            Services.AddTransient(x => DbProvider);

            RebuildServiceProvider();

        }

        protected void RebuildServiceProvider()
        {
            ServiceProvider = Services.BuildServiceProvider();
        }

        public void Dispose()
        {
            if (_transaction != null)
            {
                _transaction.Rollback();
                _transaction.Dispose();
            }
            _connection.Dispose();
            Context.Dispose();
        }


        protected TService RegisterService<TService>()
            where TService : class
        {
            return RegisterService<TService, TService>();
        }

        protected TService RegisterService<TService, TImplementation>()
            where TService : class
            where TImplementation : class, TService
        {
            Services.AddTransient<TService, TImplementation>();
            RebuildServiceProvider();
            return Services.BuildServiceProvider().GetService<TService>();
        }
    }

    public abstract class DatabaseTestBase<TClass> : TestBase where TClass : class
    {
        protected TClass DbObject { get; }

        protected DatabaseTestBase()
        {
            RegisterService<DapperContext>();
            DbObject = RegisterService<TClass>();
        }
    
    }
}
