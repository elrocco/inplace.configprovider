﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace InPlace.ConfigProvider.Tests
{
    public class Test
    {
        [Fact]
        public void Test1()
        {
            var type = typeof(Temp1);
            var method = this.GetType().GetMethod("Method");
            
            
        }

        public interface Temp<T>
        {
            
        }

        public class Temp1 : Temp<int>
        {
        }

        public void Method<T>(Temp<T> t)
        {
            Console.WriteLine(typeof(T).Name);
        }
    }


}
