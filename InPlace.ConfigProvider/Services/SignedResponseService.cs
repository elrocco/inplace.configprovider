﻿using InPlace.ConfigProvider.Model;
using InPlace.ConfigProvider.Startup;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace InPlace.ConfigProvider.Services
{
    public interface ISignedResponseService : IService
    {
        SignedResponse<T> GetSignedResponse<T>(T response);
    }
    public class SignedResponseService : ISignedResponseService
    {
        private readonly AppSettings _appSettings;

        public SignedResponseService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public SignedResponse<T> GetSignedResponse<T>(T response)
        {
            var expiry = DateTimeOffset.Now.Add(TimeSpan.FromMinutes(5));
            string dataToSign = $"{expiry.ToString("yyyyMMddHHmmss+zzz")}|{JsonConvert.SerializeObject(response)}";
            var hash = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(dataToSign));
            var rsaParams = JsonConvert.DeserializeObject<RSAParameters>(Encoding.UTF8.GetString(Convert.FromBase64String(_appSettings.SignaturePrivateKey)));
            string signature;
            using (var rsa = new RSACryptoServiceProvider(2048))
            {
                rsa.PersistKeyInCsp = false;
                rsa.ImportParameters(rsaParams);
                var rsaFormatter = new RSAPKCS1SignatureFormatter(rsa);
                rsaFormatter.SetHashAlgorithm("SHA256");
                signature = Convert.ToBase64String(rsaFormatter.CreateSignature(hash));
            }

            return new SignedResponse<T>
            {
                Data = response,
                Expiry = expiry,
                Signature = signature
            };
        }
    }
}
