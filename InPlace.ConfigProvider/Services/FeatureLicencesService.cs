﻿using InPlace.ConfigProvider.Model;
using InPlace.ConfigProvider.Queries;
using InPlace.ConfigProvider.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InPlace.ConfigProvider.Services
{
    public interface IFeatureLicencesService : IService
    {
        SignedResponse<TenantLicenceResponse> FeatureLicenceResponse(string tenantMask, string[] featureCodes);
    }

    public class FeatureLicencesService : IFeatureLicencesService
    {
        private readonly IFeatureLicenceQuery _featureLicenceQuery;
        private readonly ITenantQuery _tenantQuery;
        private readonly ISignedResponseService _signedResponseService;

        public FeatureLicencesService(IFeatureLicenceQuery featureLicenceQuery, 
            ITenantQuery tenantQuery,
            ISignedResponseService signedResponseService)
        {
            _featureLicenceQuery = featureLicenceQuery;
            _tenantQuery = tenantQuery;
            _signedResponseService = signedResponseService;
        }

        public SignedResponse<TenantLicenceResponse> FeatureLicenceResponse(string tenantMask, string[] featureCodes)
        {
            var tenantCode = new TenantCode(tenantMask);
            var validLicences = _featureLicenceQuery.Execute(new FeatureLicenceCriteria
            {
                TenantCode = tenantCode,
                FeatureCodes = featureCodes.ToList(),
                NotExpired = true
            })
            .Select(lic => new TenantValidLicence(lic))
            .ToList() ;

            var tenants = _tenantQuery
                  .Execute(new TenantCriteria { TenantCode = tenantCode })
                  .Select(t => new TenantCode(t.Region, t.Client, t.Environment))
                  .ToList();

            var result = new TenantLicenceResponse
            {
                TenantMask = tenantCode.ToString(),
                TenantLicences = tenants.ToDictionary(x => x.ToString(), x => LicencesForTenant(validLicences, x)),
            };
            return _signedResponseService.GetSignedResponse(result);
        }

        private string[] LicencesForTenant(List<TenantValidLicence> licences, TenantCode tenantCode)
        {
            var tenantCodes = TenantCodeHelper.PermeateTenantCode(tenantCode);
            return licences.Where(x => tenantCodes.ContainsKey(x.TenantCode))
                .Select(x => x.FeatureCode)
                .Distinct()
                .ToArray();
        }
    }
}
