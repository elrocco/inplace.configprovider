﻿using System;
using System.Collections.Generic;
using System.Linq;
using InPlace.ConfigProvider.Model;
using InPlace.ConfigProvider.Queries;

namespace InPlace.ConfigProvider.Services
{
    public interface ISettingValuesService : IService
    {
        SingleTenantResponse RetrieveSingleTenantSettings(string tenantCode, string[] keys);
        MultiTenantResponse RetrieveMultiTenantSettings(string tenantCode, string[] keys);
    }

    public class SettingValuesService : ISettingValuesService
    {
        private readonly ISettingValueQuery _settingValueQuery;
        private readonly ITenantQuery _tenantQuery;
        

        public SettingValuesService(ISettingValueQuery settingValueQuery, ITenantQuery tenantQuery)
        {
            _settingValueQuery = settingValueQuery;
            _tenantQuery = tenantQuery;
        }

        public SingleTenantResponse RetrieveSingleTenantSettings(string tenantCode, string[] keys)
        {
            var tc = new TenantCode(tenantCode);
            var criteria = new SettingValueCriteria { SettingsKeys = keys.ToList(), TenantCode = tc };
            var settings = _settingValueQuery.Execute(criteria).Select(sv => new TenantKeyPair(sv)).ToList();
            var settingsForTenant = SquashForClientCode(settings, tc);
            return new SingleTenantResponse
            {
                TenantCode = tc.ToString(),
                Settings = settingsForTenant
            };
        }

        public MultiTenantResponse RetrieveMultiTenantSettings(string tenantCode, string[] keys)
        {
            var tc = new TenantCode(tenantCode);

            var settings = _settingValueQuery
                .Execute(new SettingValueCriteria { TenantCode = tc, SettingsKeys = keys.ToList() })
                .Select(sv => new TenantKeyPair(sv))
                .ToList();

            var tenants = _tenantQuery
                .Execute(new TenantCriteria { TenantCode = tc })
                .Select(t => new TenantCode(t.Region, t.Client, t.Environment))
                .ToList();

            var result = new MultiTenantResponse
            {
                TenantCode = tc.ToString(),
                Settings = tenants.ToDictionary(x => x.ToString(), x => SquashForClientCode(settings, x))
            };

            return result;
        }

        private Dictionary<string, string> SquashForClientCode(List<TenantKeyPair> keyPairs, TenantCode tenantCode)
        {
            var keyPresidence = GetPermeatedTenantCodes(tenantCode);
            return keyPairs
                .Where(x => keyPresidence.ContainsKey(x.TenantCode))
                .GroupBy(x => x.KeyValue.Key)
                .Select(x => x.OrderBy(y => keyPresidence[y.TenantCode]).First().KeyValue)
                .ToDictionary(x => x.Key, x => x.Value);
        }

        private Dictionary<TenantCode, int> GetPermeatedTenantCodes(TenantCode tc)
        {
            var tenantCodes = new List<TenantCode> { tc };
            if (!new[] { "", "PROD", "NONPROD" }.Contains(tc.Environment))
                tenantCodes.Add(new TenantCode(tc.Region, tc.Client, "NONPROD"));

            if (tc.Client != "")
            {
                tenantCodes.Add(new TenantCode(tc.Region, "", tc.Environment));
                if (!new [] { "", "PROD", "NONPROD"}.Contains(tc.Environment))
                    tenantCodes.Add(new TenantCode(tc.Region, "", "NONPROD"));
            }

            if (tc.Environment != null)
            {
                tenantCodes.Add(new TenantCode(tc.Region, "", ""));
            }

            return Enumerable.Range(1, tenantCodes.Count).ToDictionary(x => tenantCodes[x - 1], x => x);
        }

  
    }


}
