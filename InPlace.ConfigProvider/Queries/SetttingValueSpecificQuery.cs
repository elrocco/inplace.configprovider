﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InPlace.ConfigProvider.Model;
using InPlace.Packages.CommandQuery;
using InPlace.Packages.Database.Dapper;

namespace InPlace.ConfigProvider.Queries
{
    public interface ISettingValueSpecificQuery : IQuery<List<KeyValue>, TenantCode>
    {
    }

    public class SetttingValueSpecificQuery : ISettingValueSpecificQuery
    {
        private readonly DapperContext _context;

        public SetttingValueSpecificQuery(DapperContext context)
        {
            _context = context;
        }

        public List<KeyValue> Execute(TenantCode criteria)
        {
            var sql = @"
SELECT SettingKey [Key], Value
FROM dbo.SettingValue 
WHERE COALESCE(Region, '') = @pRegion
AND COALESCE(Client, '') = @pClient
AND COALESCE(Environment, '') = @pEnvironment
";
            return _context.Query<KeyValue>(sql,
                new
                {
                    pRegion = criteria.Region,
                    pClient = criteria.Client,
                    pEnvironment = criteria.Environment
                })
                .ToList(); 

        }
    }
}
