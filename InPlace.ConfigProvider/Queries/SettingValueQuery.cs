﻿using System.Collections.Generic;
using System.Linq;
using InPlace.ConfigProvider.Model;
using InPlace.Packages.CommandQuery;
using InPlace.Packages.Database.Dapper;

namespace InPlace.ConfigProvider.Queries
{
    public interface ISettingValueQuery : IQuery<List<SettingValue>, SettingValueCriteria>
    {
    }

    public class SettingValueQuery : ISettingValueQuery
    {
        private readonly DapperContext _context;

        public SettingValueQuery(DapperContext context)
        {
            _context = context;
        }
        
        public List<SettingValue> Execute(SettingValueCriteria criteria)
        {
            var whereExpr = new List<string>();
            if (criteria.SettingsKeys?.Any()??false)
                whereExpr.Add("SettingKey IN @pSettingKeys");

            if (criteria.TenantCode.Region != "")
                whereExpr.Add("COALESCE(Region, '') IN ('', @pRegion)");

            if (criteria.TenantCode.Client != "")
                whereExpr.Add("COALESCE(Client, '') IN ('', @pClient)");

            if (criteria.TenantCode.Environment != "")
            {
                if (criteria.TenantCode.Environment == "NONPROD")
                    whereExpr.Add("COALESCE(Environment, '') <> 'PROD'");
                else
                    whereExpr.Add("COALESCE(Environment, '') IN ('', @pEnvironment)");

            }

            var query = $@"
SELECT Region, Client, Environment, SettingKey, Value
FROM dbo.SettingValue
where {ExpressionHelper.AndTogether(whereExpr)}";

            return _context.Query<SettingValue>(query,
                new
                {
                    pSettingKeys = criteria.SettingsKeys,
                    pRegion = criteria.TenantCode.Region,
                    pClient = criteria.TenantCode.Client,
                    pEnvironment = criteria.TenantCode.Environment
                }).ToList() ;
        }
        
        
    }

    public class SettingValueCriteria
    {
        public List<string> SettingsKeys { get; set; }
        public TenantCode TenantCode { get; set; }
    }
}
