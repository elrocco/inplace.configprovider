﻿using System;
using System.Collections.Generic;
using System.Linq;
using InPlace.ConfigProvider.Model;

namespace InPlace.ConfigProvider.Queries
{
    public static class ExpressionHelper
    {
        public static string AndTogether(List<string> expressions)
        {
            if (!expressions.Any())
                return "1=1";
            return string.Join(" and ", expressions.Where(x => !string.IsNullOrEmpty(x)).Select(exp => $"({exp})"));
        }

        public static string OrTogether(List<string> expressions)
        {
            if (!expressions.Any())
                return "1=1";
            return string.Join(" or ", expressions.Where(x => !string.IsNullOrEmpty(x)).Select(exp => $"({exp})"));
        }
    }
}
