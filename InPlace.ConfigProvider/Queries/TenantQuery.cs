﻿using System.Collections.Generic;
using System.Linq;
using InPlace.ConfigProvider.Model;
using InPlace.Packages.CommandQuery;
using InPlace.Packages.Database.Dapper;

namespace InPlace.ConfigProvider.Queries
{
    public interface ITenantQuery : IQuery<List<Tenant>, TenantCriteria>
    {
    }

    public class TenantQuery : ITenantQuery
    {
        private readonly DapperContext _context;

        public TenantQuery(DapperContext context)
        {
            _context = context;
        }


        public List<Tenant> Execute(TenantCriteria criteria)
        {
            var whereExpr = new List<string>{"1=1"};

            if (criteria.TenantCode.Region != "")
                whereExpr.Add("Region = @pRegion");

            if (criteria.TenantCode.Client != "")
                whereExpr.Add("Client = @pClient");

            if (criteria.TenantCode.Environment != "")
            {
                if (criteria.TenantCode.Environment == "NONPROD")
                    whereExpr.Add("Environment <> 'PROD'");
                else
                    whereExpr.Add("Environment = @pEnvironment");

            }
            var query = $@"
SELECT Region, Client, Environment
FROM dbo.Tenant
WHERE {ExpressionHelper.AndTogether(whereExpr)}";

            return _context.Query<Tenant>(query,
                new
                {
                    pRegion = criteria.TenantCode.Region,
                    pClient = criteria.TenantCode.Client,
                    pEnvironment = criteria.TenantCode.Environment
                }).ToList();
        }
    }

    public class TenantCriteria
    {
        public TenantCode TenantCode {get;set;}
    }
}
