﻿using InPlace.ConfigProvider.Model;
using InPlace.Packages.CommandQuery;
using InPlace.Packages.Database.Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace InPlace.ConfigProvider.Queries
{
    public interface IFeatureLicenceQuery : IQuery<List<FeatureLicence>, FeatureLicenceCriteria> { }

    public class FeatureLicenceQuery : IFeatureLicenceQuery
    {
        private readonly DapperContext _context;
        
        public FeatureLicenceQuery(DapperContext context)
        {
            _context = context;
        }

        public List<FeatureLicence> Execute(FeatureLicenceCriteria criteria)
        {
            var whereExpr = new List<string>();
            
            if (criteria.FeatureCodes?.Any()??false)
                whereExpr.Add("[FeatureCode] IN @pFeatureCodes");

            if (criteria.TenantCode.Region != "")
                whereExpr.Add("COALESCE(Region, '') IN ('', @pRegion)");

            if (criteria.TenantCode.Client != "")
                whereExpr.Add("COALESCE(Client, '') IN ('', @pClient)");

            if (criteria.TenantCode.Environment != "")
            {
                if (criteria.TenantCode.Environment == "NONPROD")
                    whereExpr.Add("COALESCE(Environment, '') <> 'PROD'");
                else
                    whereExpr.Add("COALESCE(Environment, '') IN ('', @pEnvironment)");
            }

            if (criteria.NotExpired)
                whereExpr.Add("(ExpiryDate IS NULL OR ExpiryDate > SYSDATETIMEOFFSET())");

            var query = $@"
SELECT FeatureLicenceId, Region, Client, Environment, FeatureCode, ExpiryDate
FROM dbo.FeatureLicence 
where {ExpressionHelper.AndTogether(whereExpr)}";

            return _context.Query<FeatureLicence>(query,
                new
                {
                    pFeatureCodes = criteria.FeatureCodes,
                    pRegion = criteria.TenantCode.Region,
                    pClient = criteria.TenantCode.Client,
                    pEnvironment = criteria.TenantCode.Environment
                }).ToList();
        }
    }

    public class FeatureLicenceCriteria
    {
        public List<string> FeatureCodes { get; set; }
        public TenantCode TenantCode { get; set; }
        public bool NotExpired { get; set; }
    }
}
