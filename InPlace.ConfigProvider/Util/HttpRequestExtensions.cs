﻿using Microsoft.AspNetCore.Http;
using System;

namespace InPlace.ConfigProvider.Utils
{
    public static class HttpRequestExtensions
    {
        public static string[] QueryList(this HttpRequest req, string key)
        {
            return (req.Query[key].ToString()).Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
