﻿using InPlace.ConfigProvider.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InPlace.ConfigProvider.Util
{
    public static class TenantCodeHelper
    {
        public static Dictionary<TenantCode, int> PermeateTenantCode(TenantCode tc)
        {
            var tenantCodes = new List<TenantCode> { tc };
            if (tc.Environment != "")
            {
                if (!new[] { "", "PROD", "NONPROD" }.Contains(tc.Environment))
                    tenantCodes.Add(new TenantCode(tc.Region, tc.Client, "NONPROD"));

                tenantCodes.Add(new TenantCode(tc.Region, tc.Client, ""));
            }
            

            
            if (tc.Client != "")
            {
                tenantCodes.Add(new TenantCode(tc.Region, "", tc.Environment));
                if (!new[] { "", "PROD", "NONPROD" }.Contains(tc.Environment))
                    tenantCodes.Add(new TenantCode(tc.Region, "", "NONPROD"));
            }

            if (tc.Environment != "")
            {
                tenantCodes.Add(new TenantCode(tc.Region, "", ""));
            }

            return Enumerable.Range(1, tenantCodes.Count).ToDictionary(x => tenantCodes[x - 1], x => x);
        }

        //public static Dictionary<string, string> SquashCollectionForClientCode<T>(List<T> items, TenantCode tenantCode,
        //    Func<T, string> keyFunc, Func<T, string> valueFunc)
        //{
        //    var keyPresidence = PermeateTenantCode(tenantCode);
        //    return items
        //        .Where(x => keyPresidence.ContainsKey(x.TenantCode))
        //        .GroupBy(x => x.KeyValue.Key)
        //        .Select(x => x.OrderBy(y => keyPresidence[y.TenantCode]).First().KeyValue)
        //        .ToDictionary(x => x.Key, x => x.Value);
        //}
    }
}
