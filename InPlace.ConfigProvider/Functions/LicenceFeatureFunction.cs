
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using InPlace.ConfigProvider.Injection;
using InPlace.ConfigProvider.Services;
using InPlace.ConfigProvider.Utils;

namespace InPlace.ConfigProvider.Functions
{
    public static class LicenceFeatureFunction
    {
        [FunctionName("LicenceFeatureController")]
        public static IActionResult Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "licences/{tenantCode}")]HttpRequest req, string tenantCode, ILogger log)
        {
            var licenceService = ObjectFactory.Get<IFeatureLicencesService>();
            var result = licenceService.FeatureLicenceResponse(tenantCode,req.QueryList("features"));
            return new OkObjectResult(result);
        }
    }
}
