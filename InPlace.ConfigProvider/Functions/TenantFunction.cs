﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InPlace.ConfigProvider.Commands;
using InPlace.ConfigProvider.Injection;
using InPlace.ConfigProvider.Model;
using InPlace.ConfigProvider.Queries;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;

namespace InPlace.ConfigProvider.Functions
{
    public static class TenantFunction
    {
        [FunctionName("GetTenants")]
        public static IActionResult GetTenants(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "tenants")]
            HttpRequest req, TraceWriter log)
        {
            var query = ObjectFactory.Get<ITenantQuery>();
            var result = query
                .Execute(new TenantCriteria { TenantCode = new TenantCode("") })
                .Select(x => new TenantCode(x.Region, x.Client,x.Environment).ToString())
                .ToList() ;
            return new OkObjectResult(result);
        }

        [FunctionName("AddTenant")]
        public static IActionResult AddTenant(
            [HttpTrigger(AuthorizationLevel.Anonymous, "put", Route = "tenants/{tenantCode}")]
            HttpRequest req, string tenantCode, TraceWriter log)
        {
            var commandHandler = ObjectFactory.Get<IAddTenantCommandHandler>();
            var result = commandHandler.Execute(new AddTenantCommand(new TenantCode(tenantCode)));
            return result.IsSuccess ? (ActionResult)new OkObjectResult(result) : new BadRequestObjectResult(result);
        }

        [FunctionName("DeleteTenant")]
        public static IActionResult DeleteTenant(
            [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "tenants/{tenantCode}")]
            HttpRequest req, string tenantCode, TraceWriter log)
        {
            var commandHandler = ObjectFactory.Get<IDeleteTenantCommandHandler>();
            var result = commandHandler.Execute(new DeleteTenantCommand(new TenantCode(tenantCode)));
            return result.IsSuccess ? (ActionResult)new OkObjectResult(result) : new BadRequestObjectResult(result);
        }

    }
}
