using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Http;
using InPlace.ConfigProvider.Commands;
using InPlace.ConfigProvider.Utils;
using InPlace.ConfigProvider.Injection;
using InPlace.ConfigProvider.Model;
using InPlace.ConfigProvider.Queries;
using InPlace.ConfigProvider.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;

namespace InPlace.ConfigProvider.Functions
{
    public static class SettingsFunction
    {
        [FunctionName("GetSettings")]
        public static IActionResult GetSettingsMulti([HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "settings/{tenantCode}")]HttpRequest req, string tenantCode, TraceWriter log)
        {

            var settingsService = ObjectFactory.Get<ISettingValuesService>();
            var result = settingsService.RetrieveMultiTenantSettings(tenantCode, req.QueryList("keys"));
            return (ActionResult)new OkObjectResult(result);
        }

        [FunctionName("GetSettingsExtact")]
        public static IActionResult GetSettings(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "settings/exact/{tenantCode}")]
            HttpRequest req, string tenantCode, TraceWriter log)
        {
            var settingsQuery = ObjectFactory.Get<ISettingValueSpecificQuery>();
            var result = settingsQuery.Execute(new TenantCode(tenantCode));
            return (ActionResult)new OkObjectResult(result);
        }

        [FunctionName("SetSettings")]
        public static IActionResult SetSettings(
            [HttpTrigger(AuthorizationLevel.Anonymous, "put", Route = "settings/{tenantCode}")]
            HttpRequest req, string tenantCode, TraceWriter log)
        {
            var requestBody = new StreamReader(req.Body).ReadToEnd();
            var body = JsonConvert.DeserializeObject<Dictionary<string, string>>(requestBody);
            var command = new SetSettingValuesCommand
            {
                TenantCode = new TenantCode(tenantCode),
                Settings = body.Select(x => new KeyValue(x.Key, x.Value)).ToList()
            };
            var commandHandler = ObjectFactory.Get<ISetSettingValuesCommandHandler>();
            var result = commandHandler.Execute(command);
            return result.IsSuccess ? (ActionResult)new OkObjectResult(result) : new BadRequestObjectResult(result);
        }

        [FunctionName("DeleteSettings")]
        public static IActionResult DeleteSettings(
            [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "settings/{tenantCode}")]
            HttpRequest req, string tenantCode, TraceWriter log)
        {
            var requestBody = new StreamReader(req.Body).ReadToEnd();
            var body = JsonConvert.DeserializeObject<List<string>>(requestBody);
            var command = new DeleteSettingsValuesCommand
            {
                TenantCode = new TenantCode(tenantCode),
                SettingKeys = body
            };
            var commandHandler = ObjectFactory.Get<IDeleteSettingsValuesCommandHandler>();
            var result = commandHandler.Execute(command);
            return result.IsSuccess ? (ActionResult)new OkObjectResult(result) : new BadRequestObjectResult(result);
        }
    }
}

