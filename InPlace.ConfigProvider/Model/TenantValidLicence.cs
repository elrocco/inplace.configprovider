﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InPlace.ConfigProvider.Model
{
    public class TenantValidLicence
    {
        public TenantCode TenantCode {get;set;}
        public string FeatureCode { get; set; }

        public TenantValidLicence(FeatureLicence lic)
        {
            TenantCode = new TenantCode(lic.Region, lic.Client, lic.Environment);
            FeatureCode = lic.FeatureCode;
        }
    }
}
