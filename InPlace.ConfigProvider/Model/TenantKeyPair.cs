﻿namespace InPlace.ConfigProvider.Model
{
    public class TenantKeyPair
    {
        public TenantCode TenantCode { get; }
        public KeyValue KeyValue { get;  }

        public TenantKeyPair(SettingValue sv)
        {
            TenantCode = new TenantCode(sv.Region, sv.Client, sv.Environment);
            KeyValue = new KeyValue(sv.SettingKey, sv.Value);
        }
    }
}
