﻿using System.Collections.Generic;

namespace InPlace.ConfigProvider.Model
{
    public class MultiTenantResponse
    {
        public string TenantCode { get; set; }
        public Dictionary<string, Dictionary<string, string>> Settings {get;set;}
    }
}
