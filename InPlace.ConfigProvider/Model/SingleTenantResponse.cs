﻿using System.Collections.Generic;

namespace InPlace.ConfigProvider.Model
{
    public class SingleTenantResponse
    {
        public string TenantCode { get; set; }
        public Dictionary<string,string> Settings { get; set; }
    }
}
