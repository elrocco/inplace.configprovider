﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InPlace.ConfigProvider.Model
{
    public class FeatureLicence
    {
        public int FeatureLicenceId { get; set; }
        public string Region { get; set; }
        public string Client { get; set; }
        public string Environment { get; set; }
        public string FeatureCode { get; set; }
        public DateTimeOffset? ExpiryDate { get; set; }
    }
}
