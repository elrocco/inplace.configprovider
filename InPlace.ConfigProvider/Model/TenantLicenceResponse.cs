﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InPlace.ConfigProvider.Model
{
    public class TenantLicenceResponse
    {
        public string TenantMask { get; set; }
        public Dictionary<string, string[]> TenantLicences { get; set; }
    }
}
