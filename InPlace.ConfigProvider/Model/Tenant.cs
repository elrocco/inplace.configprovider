﻿namespace InPlace.ConfigProvider.Model
{
    public class Tenant
    {
        public int TenantId { get; set; }
        public string Region { get; set; }
        public string Client { get; set; }
        public string Environment { get; set; }
    }
}
