﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InPlace.ConfigProvider.Model
{
    public class SignedResponse<T> 
    {
        public T Data { get; set; }
        public DateTimeOffset Expiry { get; set; }
        public string Signature { get; set; }
    }
}
