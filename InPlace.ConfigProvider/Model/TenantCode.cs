﻿using System;

namespace InPlace.ConfigProvider.Model
{
    public class TenantCode
    {
        public string Region { get; }
        public string Client { get; }
        public string Environment { get; }

        public TenantCode(string region, string client, string environment)
        {
            Region = region??"";
            Client = client??"";
            Environment = environment??"";
        }

        public TenantCode(string tenantCode)
        {
            var parts = tenantCode.ToUpper().Split('-');
            Func<string[], int, string> get = (arr,idx) => arr.Length > idx ? arr[idx] ?? "" : "";
            Region = get(parts, 0);
            Client = get(parts, 1);
            Environment = get(parts, 2);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Region.GetHashCode() ^ 13) * (Client.GetHashCode() ^ 17) * (Environment.GetHashCode() ^ 21);
            }
        }

        public override bool Equals(object o)
        {
            return o is TenantCode && o.GetHashCode() == GetHashCode();
        }

        public override string ToString()
        {
            return $"{Region}-{Client}-{Environment}".Trim('-');
        }
    }
}
