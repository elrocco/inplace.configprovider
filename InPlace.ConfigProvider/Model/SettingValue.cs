﻿namespace InPlace.ConfigProvider.Model
{
    public class SettingValue
    {
        public int SettingValueId { get; set; }
        public string Region { get; set; }
        public string Client { get; set; }
        public string Environment { get; set; }
        public string SettingKey { get; set; }
        public string Value { get; set; }
    }
}
