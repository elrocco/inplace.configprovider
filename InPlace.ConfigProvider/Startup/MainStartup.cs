﻿using Microsoft.Extensions.DependencyInjection;

namespace InPlace.ConfigProvider.Startup
{
    public static class MainStartup
    {
        public static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            services
                .AddSettings()
                .AddAppServices()
                .AddDbContext()
                ;
            return services;
        }
    }
}
