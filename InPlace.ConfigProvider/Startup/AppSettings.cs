﻿namespace InPlace.ConfigProvider.Startup
{
    public class AppSettings
    {
        public string ConnectionString { get; set; }
        public string CosmosConnectionString { get; set; } 
        public string SignaturePrivateKey { get; set; }
    }
}
