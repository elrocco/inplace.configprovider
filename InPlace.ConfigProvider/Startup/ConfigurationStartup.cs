﻿using System.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace InPlace.ConfigProvider.Startup
{
    public static class ConfigurationStartup
    {
        public static IServiceCollection AddSettings(this IServiceCollection services)
        {
            var vals = ConfigurationManager.AppSettings;
            var config = new ConfigurationBuilder()
                .AddEnvironmentVariables()
                .Build();

            services
                .AddOptions()
                .Configure<AppSettings>(config);

            return services;
        }
    }
}
