﻿using InPlace.ConfigProvider.Services;
using InPlace.Packages.CommandQuery;
using InPlace.Packages.Injection;
using Microsoft.Extensions.DependencyInjection;

namespace InPlace.ConfigProvider.Startup
{
    public static class AppServicesStartup
    {
        public static IServiceCollection AddAppServices(this IServiceCollection services)
        {
            services
                .RegisterAssemblies(DependancyLifetime.Transient, new[] { "Inplace.ConfigProvider" }, typeof(IQuery), typeof(ICommandHandler), typeof(IService))
                ;
            return services;
        }
    }
}
