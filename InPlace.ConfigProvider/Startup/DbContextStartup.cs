﻿using System.Data.SqlClient;
using InPlace.Packages.Database;
using InPlace.Packages.Database.Dapper;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace InPlace.ConfigProvider.Startup
{
    public static class DbContextStartup
    {
        public static IServiceCollection AddDbContext(this IServiceCollection services)
        {
            services
                .AddTransient<DapperContext>()
                .AddScoped<IDbProvider>(provider =>
                {
                    var appSettings = provider.GetService<IOptions<AppSettings>>().Value;
                    return new DbProvider
                    {
                        DbConnection = new SqlConnection(appSettings.ConnectionString)
                    };
                })
                ;

            return services;
        }
    }
}
