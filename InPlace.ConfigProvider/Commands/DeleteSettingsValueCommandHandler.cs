﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using FastMember;
using InPlace.ConfigProvider.Model;
using InPlace.Packages.CommandQuery;
using InPlace.Packages.Database.Dapper;
using InPlace.Packages.ResultModels;

namespace InPlace.ConfigProvider.Commands
{
    public interface IDeleteSettingsValuesCommandHandler : ICommandHandler<DeleteSettingsValuesCommand>
    {
    }

    public class DeleteSettingsValuesCommandHandler : IDeleteSettingsValuesCommandHandler
    {
        private readonly DapperContext _context;

        public DeleteSettingsValuesCommandHandler(DapperContext context)
        {
            _context = context;
        }

        public Result Execute(DeleteSettingsValuesCommand command)
        {
            string tmpTableName = "#DelSettingValues";
            _context.Execute($@"
if (object_id('tempdb..{tmpTableName}') is not null)
    drop table {tmpTableName};
create table {tmpTableName} ([Key] varchar(100));");

            using (var sbc = new SqlBulkCopy((SqlConnection)_context.DbConnection, SqlBulkCopyOptions.Default, (SqlTransaction)_context.DbTransaction))
            {
                sbc.DestinationTableName = tmpTableName;
                sbc.BatchSize = command.SettingKeys.Count;
                var members = typeof(DeleteSettingsKeyDto).GetProperties().Select(x => x.Name).ToArray();
                foreach (var prop in members)
                    sbc.ColumnMappings.Add(prop, prop);
                using (var reader =
                    ObjectReader.Create(command.SettingKeys.Select(x => new DeleteSettingsKeyDto(x)).ToList(), members))
                {
                    sbc.WriteToServer(reader);
                }
            }

            _context.Execute($@"
delete sv
from dbo.SettingValue sv
join {tmpTableName} t on t.SettingKey = sv.SettingKey
where isnull(sv.Region, '') = @tcRegion
and isnull(sv.Client, '') = @tcClient
and isnull(sv.Environment, '') = @tcEnvironment");

            _context.Execute($"if (object_id('tempdb..{tmpTableName}') is not null) drop table {tmpTableName}");

            return new Result();
        }

        private class DeleteSettingsKeyDto
        {
            public string SettingKey { get; }

            public DeleteSettingsKeyDto(string key)
            {
                SettingKey = key;
            }
        }
    }

    public class DeleteSettingsValuesCommand
    {
        public TenantCode TenantCode { get; set; }
        public List<string> SettingKeys { get; set; }
    }
}
