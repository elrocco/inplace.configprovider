﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InPlace.ConfigProvider.Model;
using InPlace.Packages.CommandQuery;
using InPlace.Packages.Database.Dapper;
using InPlace.Packages.ResultModels;

namespace InPlace.ConfigProvider.Commands
{
    public interface IAddTenantCommandHandler : ICommandHandler<AddTenantCommand>
    {
    }

    public class AddTenantCommandHandler : IAddTenantCommandHandler
    {
        private readonly DapperContext _context;

        public AddTenantCommandHandler(DapperContext context)
        {
            _context = context;
        }

        public Result Execute(AddTenantCommand command)
        {
            var existsCheck = _context.Query<int>("select COUNT(*) from Tenant where Region = @Region and Client = @Client and Environment = @Environment", command.TenantCode).First();
            var result = new Result();
            if (existsCheck != 0)
                result.AddError($"Tenant {command.TenantCode} already exists");
            if (result.IsSuccess)
            {
                _context.Execute("insert into Tenant (Region, Client, Environment) values (@Region, @Client, @Environment)", command.TenantCode);
            }
            return result;
        }
    }

    public class AddTenantCommand
    {
        public TenantCode TenantCode { get; }

        public AddTenantCommand(TenantCode tenantCode)
        {
            TenantCode = tenantCode;
        }
    }
}
