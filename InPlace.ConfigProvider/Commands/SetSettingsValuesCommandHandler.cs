﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using FastMember;
using InPlace.ConfigProvider.Model;
using InPlace.Packages.CommandQuery;
using InPlace.Packages.Database.Dapper;
using InPlace.Packages.ResultModels;

namespace InPlace.ConfigProvider
{
    public interface ISetSettingValuesCommandHandler : ICommandHandler<SetSettingValuesCommand>
    {
    }

    public class SetSettingValuesCommandHandler : ISetSettingValuesCommandHandler
    {
        private readonly DapperContext _context;

        public SetSettingValuesCommandHandler(DapperContext context)
        {
            _context = context;
        }

        public Result Execute(SetSettingValuesCommand command)
        {
            string tmpTableName = "#SetSettingsValues";
            _context.Execute($@"
if (object_id('tempdb..{tmpTableName}') is not null)
    drop table {tmpTableName};
create table {tmpTableName} ([Key] varchar(100), [Value] nvarchar(max))");

            using (var sbc = new SqlBulkCopy((SqlConnection) _context.DbConnection, SqlBulkCopyOptions.Default, (SqlTransaction)_context.DbTransaction))
            {
                sbc.DestinationTableName = tmpTableName;
                sbc.BatchSize = command.Settings.Count;
                var members = typeof(KeyValue).GetProperties().Select(x => x.Name).ToArray();
                foreach (var prop in members)
                    sbc.ColumnMappings.Add(prop, prop);
                using (var reader = ObjectReader.Create(command.Settings, members))
                {
                    sbc.WriteToServer(reader);
                }
            }

            _context.Execute($@"
merge into dbo.SettingValue tgt
using (select * from {tmpTableName}) src
on isnull(tgt.Region, '') = @tcRegion
and isnull(tgt.Client, '') = @tcClient
and isnull(tgt.Environment, '') = @tcEnvironment
and src.[Key] = tgt.SettingKey
when matched 
then update 
set Value = src.Value
when not matched by target 
then insert
(Region, Client, Environment, SettingKey, Value)
values (nullif(@tcRegion,''), nullif(@tcClient,''), nullif(@tcEnvironment, ''), src.[Key], src.[Value]);", 
                new
                {
                    tcRegion = command.TenantCode.Region,
                    tcClient = command.TenantCode.Client,
                    tcEnvironment = command.TenantCode.Environment
                });

            _context.Execute($"if (object_id('tempdb..{tmpTableName}') is not null) drop table {tmpTableName};");

            return new Result();
        }
    }


    public class SetSettingValuesCommand
    {
        public TenantCode TenantCode { get; set; }
        public List<KeyValue> Settings { get; set; }
    }
}
