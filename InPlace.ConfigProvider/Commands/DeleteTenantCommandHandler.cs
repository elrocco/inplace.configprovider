﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InPlace.ConfigProvider.Model;
using InPlace.Packages.CommandQuery;
using InPlace.Packages.Database.Dapper;
using InPlace.Packages.ResultModels;

namespace InPlace.ConfigProvider.Commands
{
    public interface IDeleteTenantCommandHandler : ICommandHandler<DeleteTenantCommand>
    {
    }

    public class DeleteTenantCommandHandler : IDeleteTenantCommandHandler
    {
        private readonly DapperContext _context;

        public DeleteTenantCommandHandler(DapperContext context)
        {
            _context = context;
        }

        public Result Execute(DeleteTenantCommand command)
        {
            var result = new Result();
            bool existsCheck = _context.Query<int>("select COUNT(*) from Tenant where Region = @Region and Client = @Client and Environment = @Environment", command.TenantCode).First() > 0;
            if (!existsCheck)
                result.AddError($"Tenant {command.TenantCode} not found");
            if (result.IsSuccess)
                _context.Execute("delete from dbo.Tenant where Region = @Region and Client = @Client and Environment = @Environment", command.TenantCode);
            return result;
        }
    }

    public class DeleteTenantCommand
    {
        public TenantCode TenantCode { get; }

        public DeleteTenantCommand(TenantCode tenantCode)
        {
            TenantCode = tenantCode;
        }
    }
}
