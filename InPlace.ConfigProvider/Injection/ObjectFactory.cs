﻿using System;
using InPlace.ConfigProvider.Startup;
using Microsoft.Extensions.DependencyInjection;

namespace InPlace.ConfigProvider.Injection
{
    public static class ObjectFactory
    {
        private static IServiceProvider _serviceProvider;

        private static void Build()
        {
            if (_serviceProvider == null)
                _serviceProvider = new ServiceCollection()
                    .ConfigureServices()
                    .BuildServiceProvider();
        }

        public static TService Get<TService>()
        {
            Build();
            return _serviceProvider.GetService<TService>();
        }
    }
}
