﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using InPlace.ConfigProvider.ApiConsumer.Config;
using InPlace.ConfigProvider.ApiConsumer.ResponseModel;
using InPlace.ConfigProvider.ResponseModel;
using InPlace.Packages.ResultModels;
using Newtonsoft.Json;

namespace InPlace.ConfigProvider.ApiConsumer
{
    public class InPlaceManagerConfigFetcher
    {
        private readonly ManagedSettingsConfiguration _config;

        public InPlaceManagerConfigFetcher(ManagedSettingsConfiguration config)
        {
            _config = config;
        }

        public Result<SettingsResponse> GetSettings(params string[] keys)
        {
            using (var client = GetHttpClient())
            {
                string keysArg = keys.Length > 0 ? $"keys={string.Join(",", keys)}" : "";
                return ProcessResponse<SettingsResponse>(client.GetAsync($"api/settings/{_config.TenantMask}?{keysArg}").Result);
            }
        }

        public Result<SignedResponse<TenantLicenceResponse>> GetLicences(params string[] featureCodes)
        {
            using (var client = GetHttpClient())
            {
                string featuresArg = featureCodes.Length > 0 ? $"featurs={string.Join(",", featureCodes)}" : "";
                return ProcessResponse<SignedResponse<TenantLicenceResponse>>(client.GetAsync($"api/licences/{_config.TenantMask}?{featuresArg}").Result);
            }
        }

        private Result<T> ProcessResponse<T>(HttpResponseMessage response)
        {
            var result = new Result<T>();
            if (!response.IsSuccessStatusCode)
                result.Errors.Add($"Response did not indicate success. StatusCode={response.StatusCode}");
            if (result.IsSuccess)
            {
                var responseString = response.Content.ReadAsStringAsync().Result;
                result.Value = JsonConvert.DeserializeObject<T>(responseString);
            }
            return result;
        }

        private HttpClient GetHttpClient()
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri(_config.BaseUrl)
            };
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            return client;
        }
    }
}
