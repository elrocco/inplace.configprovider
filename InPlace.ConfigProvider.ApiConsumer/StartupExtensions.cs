﻿using InPlace.ConfigProvider.ApiConsumer.Cache;
using InPlace.ConfigProvider.ApiConsumer.Config;
using InPlace.ConfigProvider.ApiConsumer.ResponseModel;
using InPlace.ConfigProvider.ApiConsumer.Security;
using InPlace.ConfigProvider.ResponseModel;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace InPlace.ConfigProvider.ApiConsumer
{
    public static class StartupExtensions
    {
        public static IServiceCollection SetManagedConfigurationConfig(this IServiceCollection service, ManagedSettingsConfiguration config)
        {
            service.AddSingleton(config);
            service.AddScoped<TenantCodeProvider, HttpContextTenantCodeProvider>();
            return service;
        }

        public static IServiceCollection ConfigureSettings<T>(this IServiceCollection services, params string[] settings)
            where T : class
        {
            var provider = services.BuildServiceProvider();
            var config = provider.GetService<ManagedSettingsConfiguration>();
            var settingsResponse = new InPlaceManagerConfigFetcher(config).GetSettings(settings);
            if (!settingsResponse.IsSuccess)
                throw new System.Exception(settingsResponse.Errors[0]);
            services.AddSingleton(BuildSettingsCache<T>(settingsResponse.Value));
            RegisterTenantCodeScope<T>(services);
            return services;
        }

        public static IServiceCollection ConfigureFeatures<T>(this IServiceCollection services, params string[] features) where T : class, new()
        {
            var provider = services.BuildServiceProvider();
            var config = provider.GetService<ManagedSettingsConfiguration>();
            var featureCodes = typeof(T).GetProperties().Where(x => x.PropertyType == typeof(bool)).Select(x => x.Name);
            if (features.Length > 0)
                featureCodes = featureCodes.Intersect(features);
            var featuresResponse = new InPlaceManagerConfigFetcher(config).GetLicences(featureCodes.ToArray());
            if (!featuresResponse.IsSuccess)
                throw new System.Exception(featuresResponse.Errors[0]);
            services.AddSingleton(BuildFeaturesCache<T>(featuresResponse.Value));
            RegisterTenantCodeScope<T>(services);
            return services;
        }

        private static void RegisterTenantCodeScope<T>(IServiceCollection services) where T : class
        {
            services.AddScoped(sp =>
            {
                var tenantCode = sp.GetService<TenantCodeProvider>().GetTenantCode();
                var cache = sp.GetService<ManagedCache<T>>();
                T value;
                if (!cache.ManagedSettings.TryGetValue(tenantCode, out value))
                    throw new System.Exception($"Could not build {typeof(T).Name} for tenant {tenantCode}");
                return value;
            });
        }
                
        private static ManagedCache<T> BuildSettingsCache<T>(SettingsResponse response) 
        {
            var cache = new ManagedCache<T>();
            foreach(var settingSet in response.Settings)
            {
                var config = new ConfigurationBuilder()
                     .AddInMemoryCollection(settingSet.Value)
                     .Build();
                var settingsObj = config.Get<T>();
                cache.ManagedSettings.Add(settingSet.Key, settingsObj);
            }
            return cache;
        }

        private static ManagedCache<T> BuildFeaturesCache<T>(SignedResponse<TenantLicenceResponse> response)
            where T : new()
        {
            var cache = new ManagedCache<T>();
            SignatureValidator.ValidateSignature(response);
            var properties = typeof(T).GetProperties()
                .ToDictionary(x => x.Name);
            foreach(var featureSet in response.Data.TenantLicences)
            {
                var o = new T();
                foreach(var feature in featureSet.Value)
                {
                    if (properties.ContainsKey(feature))
                    {
                        properties[feature].SetValue(o, true);
                    }
                }
                cache.ManagedSettings.Add(featureSet.Key, o);
            }
            return cache;
        }
    }
}
