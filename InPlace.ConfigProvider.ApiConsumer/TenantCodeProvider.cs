﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace InPlace.ConfigProvider.ApiConsumer
{
    public interface TenantCodeProvider
    {
        string GetTenantCode();
    }
    public class HttpContextTenantCodeProvider : TenantCodeProvider
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public HttpContextTenantCodeProvider(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string GetTenantCode()
        {
            return _httpContextAccessor.HttpContext.User.FindFirst("tntid").Value;
        }
    }
}
