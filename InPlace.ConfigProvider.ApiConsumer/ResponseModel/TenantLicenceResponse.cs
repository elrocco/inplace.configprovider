﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InPlace.ConfigProvider.ApiConsumer.ResponseModel
{
    public class TenantLicenceResponse
    {
        public string TenantMask { get; set; }
        public Dictionary<string, string[]> TenantLicences { get; set; }
    }

    public class SignedResponse<T>
    {
        public T Data { get; set; }
        public DateTimeOffset ExpiryDate { get; set; }
        public string Signature { get; set; }
    }

}
