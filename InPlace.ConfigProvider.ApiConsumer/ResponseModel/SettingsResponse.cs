﻿using System.Collections.Generic;

namespace InPlace.ConfigProvider.ResponseModel
{
    public class SettingsResponse
    {
        public string TenantCode { get; set; }
        public Dictionary<string, Dictionary<string, string>> Settings {get;set;}
    }
}
