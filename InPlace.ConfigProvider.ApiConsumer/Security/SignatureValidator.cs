﻿using InPlace.ConfigProvider.ApiConsumer.ResponseModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace InPlace.ConfigProvider.ApiConsumer.Security
{
    public static class SignatureValidator
    {
        private const string _licenceResponsePublicKey = "eyJEIjpudWxsLCJEUCI6bnVsbCwiRFEiOm51bGwsIkV4cG9uZW50IjoiQVFBQiIsIkludmVyc2VRIjpudWxsLCJNb2R1bHVzIjoidzVpQVVPbTltd2FnQUlRL09EeGhsdER5R2xvVXdSM3dva3RoYXZPQm1OcmhkdDJNN1k0TWl1ckRRQzNlSlRLUUl6ZjJsc2JFYUFld2NwU29RQkZXdFg0NE1MMFhReUVacUt1cnpHZWlBbWdjQWpJUS9FVDVML3RwZkNUYmx5cVIyV0JYeG9UeWJ4Z3pPeVA2UlltZ1R5djdyTFpUOUhOanRqanU3RnQrVlhEcUQ1ZklCN2xuVUt0SkFOenhEeFl0b0hvMmFZUDc2akljNXdFelFyWHlWMUltYXRiMnFRbnR3aThTUElaZ0tqVnJRc1JjaWJSV1NnT25pMXZYc3lNVkIxcnkzZVJsL3ZSdmt1WE9ZdmZMeVpzWkk0Z3FiZWFtZGVpQnpIdVdzeGVIYWlsY1RBNkdLMk9zSEhaRXYyWTNGSHpmU29YcHJ0RW5lR0lubkovUE5RPT0iLCJQIjpudWxsLCJRIjpudWxsfQ==";

        public static void ValidateSignature<T>(SignedResponse<T> response)
        {
            if (response.ExpiryDate < DateTimeOffset.Now) throw new Exception("Response has expired");
            string dataToSign = $"{response.ExpiryDate.ToString("yyyyMMddHHmmss+zzz")}|{JsonConvert.SerializeObject(response.Data)}";
            var hash = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(dataToSign));
            var rsaParams = JsonConvert.DeserializeObject<RSAParameters>(Encoding.UTF8.GetString(Convert.FromBase64String(_licenceResponsePublicKey)));
            using (var rsa = new RSACryptoServiceProvider(2048))
            {
                rsa.PersistKeyInCsp = false;
                rsa.ImportParameters(rsaParams);
                var rsaDeformatter = new RSAPKCS1SignatureDeformatter(rsa);
                rsaDeformatter.SetHashAlgorithm("SHA256");
                bool valid = rsaDeformatter.VerifySignature(hash, Convert.FromBase64String(response.Signature));
                if (!valid) throw new Exception("Signature is invalid");
            }
        }
    }
}
