﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InPlace.ConfigProvider.ApiConsumer.Config
{
    public class ManagedSettingsConfiguration
    {
        public string TenantMask { get; set; }
        public string BaseUrl { get; set; }

        private const string _defaultBaseUrl = "https://inplace-config-test.azurewebsites.net";

        public ManagedSettingsConfiguration(string tenantMask)
        {
            TenantMask = tenantMask;
            BaseUrl = _defaultBaseUrl;
        }
    }
}
