param(
	[Parameter(Mandatory =  $false)] $connection,
	[Parameter(Mandatory = $false)] $withdata
)

$settingsFile = "$PSScriptRoot\InPlace.ConfigProvider\local.settings.json"
$testSettingsFile = "$PSScriptRoot\InPlace.ConfigProvider.Tests\appsettings.json"
$migrationsProjFolder = "$PSScriptRoot\InPlace.ConfigProvider.Migrations"
$migrationsBinFolder = "$migrationsProjFolder\bin"
$migrationsDll = "$migrationsBinFolder\InPlace.ConfigProvider.Migrations.dll"

$connectionString = "";
if($connection -eq $null)
{
	throw "No connection method provided"
}	
if ($connection -eq "app")
{
	if (-not(test-path $settingsFile))
	{
		throw "file not found: $settingsFile"
	}

	$settings = cat $settingsFile | convertfrom-json 
	$connectionString = $settings.Values.connectionString
}
elseif ($connection = "test")
{
	if (-not(test-path $testSettingsFile))
	{
		throw "file not found: $testSettingsFile"
	}
	$connectionString = (cat $testSettingsFile | convertfrom-json).ConnectionString
}
elseif ($connection.startsWith("str:"))
{
	$connectionString = $connection.substring(4)
}
if ($connection -eq "")
{
	throw "Connection method invalid"
}
echo "ConnectionString: $connectionString"

dotnet restore $migrationsProjFolder
dotnet build -c release $migrationsProjFolder -o $migrationsBinFolder
dotnet $migrationsDll $connectionString

if ($withdata -ne $null)
{
	$dataPath = "$migrationsProjFolder\TestData\$withdata.sql"
	if (-not(test-path $datapath))
	{
		throw "File not found: $datapath"
	}
	$cnn = new-object system.data.sqlclient.sqlconnection $connectionString
	try
	{
		$cnn.open()
		$cmd = $cnn.createCommand();
		$cmd.CommandText = (cat -raw $datapath)
		$cmd.ExecuteNonQuery()
	}
	catch{throw}
	finally{$cnn.dispose()}
}
