﻿create table FeatureLicence
(
	FeatureLicenceId int identity(1,1),
	Region varchar(100) null,
	Client varchar(100) null,
	Environment varchar(100) null,
	FeatureCode varchar(100) not null,
	ExpiryDate datetimeoffset null,
	primary key (FeatureLicenceId)
)