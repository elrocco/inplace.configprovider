﻿create table SettingValue
(
	SettingValueId int identity(1,1),
	Region varchar(100) null,
	Client varchar(100) null,
	Environment varchar(100) null,
	SettingKey varchar(100) not null,
	Value nvarchar(max) not null,
	primary key (SettingValueId)
);

create table Tenant
(
	TenantId int identity(1,1),
	Region varchar(100) not null,
	Client varchar(100) not null,
	Environment varchar(100) not null,
	primary key (TenantId)
);