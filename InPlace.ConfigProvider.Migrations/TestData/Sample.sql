insert into SettingValue 
(Region, Client, Environment, SettingKey, Value)
values
('AU', 'MONASH', 'DEV', 'Testing.Setting', 'SomeValue'),
('AU', 'MONASH','UAT', 'Testing.Setting', 'SomeOtherValue'),
('AU', NULL, NULL, 'Random.Setting', 'http'),
('AU', 'RMIT', 'DEV', 'Data.Setting', 'DEV'),
('AU', 'RMIT', 'UAT', 'Data.Setting', 'UAT'),
('AU', 'RMIT', 'PROD', 'Data.Setting', 'PROD'),
('AU', NULL, 'PROD', 'Data.Setting', 'PROD'),
('AU', NULL, 'NONPROD', 'Data.Setting', 'DEV')
;

insert into Tenant
(Region, Client, Environment)
values 
('AU', 'MONASH', 'DEV'),
('AU', 'MONASH', 'UAT'),
('AU', 'MONASH', 'PROD'),
('AU', 'RMIT', 'DEV'),
('AU', 'RMIT', 'UAT'),
('AU', 'RMIT', 'PROD')
;

insert into FeatureLicence
(Region, Client, Environment, FeatureCode)
values
('AU', 'RMIT', null, 'LogBook'),
('AU', 'RMIT', null, 'LogBookAd')
;
